/* eslint-disable */
import React from "react";
import { ProductCard } from "./FeaturedProducts";

export interface BlogInterface {
  id: string;
  title: string;
  published: string;
  excerpt:string;
  image: string;
}

interface Props {
  courseInfo: BlogInterface[];
}

const LatestBlogs: React.FC<Props> = ({ courseInfo }) => {
  return (
    <section className="from-blog spad">
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <div className="section-title from-blog__title">
              <h2>From The Blog</h2>
            </div>
          </div>
        </div>
        <div className="row">
          {/* blog card - list all here */}
          {courseInfo.map((c) => (
            <div className="col-lg-4 col-md-4 col-sm-6" key={`courses-${c.id}`}>
              <a href="#" className="blog__item">
                <div className="blog__item__pic">
                  <img
                    src={c.image}
                    alt=""
                    height={300}
                  />
                </div>
                <div className="blog__item__text">
                  <ul>
                    <li>
                      <i className="fa fa-calendar-o"></i> {c.published}
                    </li>
                  </ul>
                  <h5>{c.title}</h5>
                  <p>{c.excerpt}</p>
                </div>
              </a>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};

export default LatestBlogs;
