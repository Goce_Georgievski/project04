import Router, { useRouter } from "next/router";
import React, { useState } from "react";

interface Props {
  productCategoriesInfo: {
    name: string;
    slug: string;
  }[];
  productSizesInfo: {
    name: string;
    slug: string;
  }[];
}

const Filters: React.FC<Props> = ({
  productCategoriesInfo,
  productSizesInfo,
}) => {
  const router = useRouter();
  const [currentCategory, setCategory] = useState("" || router.query.type);
  const [currentSize, setCurrentSize] = useState("" || router.query.size);

  function myCategory(slugCategory: string) {
    setCategory(slugCategory);

    if (slugCategory) {
      router.push({
        pathname: "/shop",
        query: {
          ...router.query,
          type: slugCategory,
        },
      });
    } else if (currentSize) {
      router.push({
        pathname: "/shop",
        query: {
          size: currentSize,
        },
      });
    } else {
      router.push({
        pathname: "/shop",
      });
    }
  }

  function mySize(slugSize: string) {
    setCurrentSize(slugSize);

    if (slugSize) {
      router.push({
        pathname: "/shop",
        query: {
          ...router.query,
          size: slugSize,
        },
      });
    } else if (currentCategory) {
      router.push({
        pathname: "/shop",
        query: {
          type: currentCategory,
        },
      });
    } else {
      router.push({
        pathname: "/shop",
      });
    }
  }

  return (
    <div className="sidebar">
      <div className="sidebar__item">
        <h4>Department</h4>
        {/* show this button if there is a category filter selected to remove it */}
        {currentCategory && (
          <div className="mb-3">
            <button className="btn btn-danger btn-sm" onClick={() => myCategory("")}>
              Clear filter <span className="ml-1">&#10005;</span>
            </button>
          </div>
        )}

        {productCategoriesInfo.map((el) => {
          return (
            <div className="sidebar__item__size" key={`productCategoriesNames-${el.slug}`}>
              {/* toggle the active class here */}
              <label className={router.query.type == `${el.slug}` ? "active" : undefined} onClick={() => myCategory(el.slug)}>
                {el.name}
                <input type="radio" name={el.name} />
              </label>
            </div>
          );
        })}
      </div>
      <div className="sidebar__item">
        <h4>Popular Size</h4>
        {/* show this button if there is a size filter selected to remove it */}
        {currentSize && (
          <div className="mb-3">
            <button className="btn btn-danger btn-sm" onClick={() => mySize("")}>
              Clear filter <span className="ml-1">&#10005;</span>
            </button>
          </div>
        )}

        {productSizesInfo.map((size) => {
          return (
            <div className="sidebar__item__size" key={`productSizes-${size.slug}`}>
              <label className={router.query.size == `${size.slug}` ? "active" : ""} onClick={() => mySize(size.slug)}>
                {size.name}
                <input type="radio" name={size.name} />
              </label>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Filters;
