/* eslint-disable */
import Link from "next/link";
import React from "react";


interface Props {
  id:string;
  title:string;
  type: string;
  description: string;
  filename: string;
  size: string;
  price: number;
  information: string;
}
const Product: React.FC<Props> = ({description,filename,id,information,price,size,title,type}) => {
  return (
    <div className="col-lg-3 col-md-4 col-sm-6">
      <div className="product__item">
        <div className="product__item__pic">
          {/* fill out the correct url */}
          <img src={`/img/products/${filename}`} width={262} height={270} alt="" />

          <ul className="product__item__pic__hover">
            <li>
              <a href="#">
                <i className="fab fa-facebook"></i>
              </a>
            </li>
            <li>
              <a href="#">
                <i className="fa fa-retweet"></i>
              </a>
            </li>
            <li>
              <a href="#">
                <i className="fab fa-linkedin"></i>
              </a>
            </li>
          </ul>
        </div>
        {/* fill out these */}
    <Link href={`/shop/${id}`}>

        <a className="product__item__text d-block">
          <h6>{title}</h6>
          <h5>${price}</h5>
        </a>
    </Link>
      </div>
    </div>
  );
};

export default Product;
