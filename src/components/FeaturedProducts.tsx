import Link from "next/link";
import React from "react";
import Product from "./Product";

export interface ProductCard {
  id:string;
  title:string;
  type: string;
  description: string;
  filename: string;
  size: string;
  price: number;
  information: string;
}

interface Props {
  featuredProductsInfo:ProductCard[],
}

const FeaturedProducts: React.FC<Props> = ({featuredProductsInfo}) => {
  return (
    <section className="featured spad">
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <div className="section-title">
              <h2>Featured Products</h2>
            </div>
          </div>
        </div>
        <div className="row">
          {featuredProductsInfo.map(product=> (
            <Product key={`prduct-${product.id}`} id={product.id} description={product.description} filename={product.filename} information={product.information}  price={product.price} size={product.size} title={product.title} type={product.type}/>
          ))}
          
        </div>

        <div className="row">
          <div className="col text-center">
            <Link href="/shop">
              <a className="primary-btn">See all</a>
            </Link>
          </div>
        </div>
      </div>
    </section>
  );
};

export default FeaturedProducts;
