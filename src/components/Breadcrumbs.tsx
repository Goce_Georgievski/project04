import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";


interface Props {
  title?:string;
}
const Breadcrumbs: React.FC<Props> = ({title}) => {

  const {pathname} = useRouter() 
  return (
    <section className="breadcrumb-section set-bg">
      <div className="container">
        <div className="row">
          <div className="col-lg-12 text-center">
            <div className="breadcrumb__text">
              <h2>Ogani Shop</h2>
              <div className="breadcrumb__option ">
                <Link href="/">
                  <a>Home</a>
                </Link>
      { title ? <> <span className="font-weight-bold ">Shop</span> <span>- {title}</span> </> : <span>Shop</span>}
              
         
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Breadcrumbs;
