/* eslint-disable */
import Link from "next/link";
import React from "react";

interface Props {
  preTitle: string;
  title: string;
  desc: string;
  departmentInfo:string;
  productCategoriesInfo:{
    name:string;
    slug:string;
  }[],
}



const HeroSection: React.FC<Props> = ({departmentInfo,desc,preTitle,title,productCategoriesInfo}) => {
  return (
    <section className="hero">
      <div className="container">
        <div className="row">
          <div className="col-lg-3">
            <div className="hero__categories">
              <div className="hero__categories__all">
                <i className="fa fa-bars"></i>
                <span>All departments</span>
              </div>
              <div className="py-3">
                {/* list all categories here */}
                {productCategoriesInfo.map((el,i)=> (
                <Link href={`http://localhost:3000/shop?type=${el.slug}`} key={`slug-${i}`}> 
                 <div className="sidebar__item__size" >
                  <button>{el.name}</button>
                </div></Link>
                ))}
                {/* !! */}
              </div>
              {/* fill out this one */}
              <p className="mt-3">{departmentInfo}</p>
            </div>
          </div>
          <div className="col-lg-9">
            <div className="hero__item set-bg">
              <div className="hero__text w-50">
                {/* fill out these */}
                <span>{preTitle}</span>
                <h2>{title}</h2>
                <p>{desc}</p>
                {/* !! */}
                <Link href="/shop">
                  <a className="primary-btn">SHOP NOW</a>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default HeroSection;
