import type { GetStaticProps, NextPage } from "next";
import Head from "next/head";
import FeaturedProducts, { ProductCard } from "../components/FeaturedProducts";
import HeroSection from "../components/HeroSection";
import LatestBlogs, { BlogInterface } from "../components/LatestBlogs";

interface Props {
  heroSectionInfo: {
    heroSection: {
      preTitle: string;
      title: string;
      desc: string;
    };
    departmentInfo: string;
  };
  featuredProductsInfo:ProductCard[];

  productCategoriesInfo: {
    name:string;
    slug:string;
  }[]

  courseInfo:BlogInterface[],
}

const Home: NextPage<Props> = ({ heroSectionInfo ,featuredProductsInfo,productCategoriesInfo,courseInfo}) => {
  return (
    <div>
      <Head>
        <title>Homepage</title>
      </Head>

      <HeroSection title={heroSectionInfo.heroSection.title} preTitle={heroSectionInfo.heroSection.preTitle} desc={heroSectionInfo.heroSection.desc} departmentInfo={heroSectionInfo.departmentInfo} productCategoriesInfo={productCategoriesInfo} />

      <FeaturedProducts featuredProductsInfo={featuredProductsInfo} />

      <LatestBlogs courseInfo={courseInfo} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  const res = await fetch("http://localhost:5001/homepage");
  const heroSectionInfo = await res.json();

 
  const resTwo = await fetch("http://localhost:5001/productCategories");
  const productCategoriesInfo = await resTwo.json();


  const resOne = await fetch("http://localhost:5001/products?_start=1&_limit=4");
  const featuredProductsInfo = await resOne.json();

  const resThree = await fetch("http://localhost:5001/blogs")
  const courseInfo = await resThree.json()
  return {
    props: {
      heroSectionInfo,
      featuredProductsInfo,
      productCategoriesInfo,
      courseInfo,
    },
  };
};

export default Home;
