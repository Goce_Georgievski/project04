/* eslint-disable */
import type { GetStaticPaths, GetStaticProps, NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import Breadcrumbs from "../../components/Breadcrumbs";
import FeaturedProducts, {
  ProductCard,
} from "../../components/FeaturedProducts";

interface Props {
  filtered: ProductCard;
  fearutedFour:ProductCard[]
}

const ShopDetail: NextPage<Props> = ({ filtered ,fearutedFour}) => {
  return (
    <div>
      <Head>
        <title>{filtered.title}</title>
      </Head>

      <Breadcrumbs title={filtered.title} />

      <section className="product-details spad">
        <div className="container">
          <div className="row">
            <div className="col-lg-6 col-md-6">
              <div className="product__details__pic">
                <div className="product__details__pic__item">
                  <img className="product__details__pic__item--large" src={`/img/products/${filtered.filename}`} alt="image"/>
                </div>
              </div>
            </div>
            <div className="col-lg-6 col-md-6">
              <div className="product__details__text">
                <h3>{filtered.title}</h3>
                <div className="product__details__price">${filtered.price}</div>
                <p>{filtered.description}</p>
                <ul>
                  <li>
                    <b>Type</b> <span>{filtered.type}</span>
                  </li>
                  <li>
                    <b>Size</b> <span>{filtered.size}</span>
                  </li>
                  <li>
                    <b>Share on</b>
                    <div className="share">
                      <a href="#">
                        <i className="fab fa-facebook"></i>
                      </a>
                      <a href="#">
                        <i className="fab fa-twitter"></i>
                      </a>
                      <a href="#">
                        <i className="fab fa-instagram"></i>
                      </a>
                      <a href="#">
                        <i className="fab fa-pinterest"></i>
                      </a>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-12">
              <div className="product__details__tab">
                <ul className="nav nav-tabs" role="tablist">
                  <li className="nav-item">
                    <a
                      className="nav-link active"
                      data-toggle="tab"
                      href="#tabs-1"
                      role="tab"
                      aria-selected="true"
                    >
                      Description
                    </a>
                  </li>
                </ul>
                <div className="tab-content">
                  <div className="tab-pane active" id="tabs-1" role="tabpanel">
                    <div className="product__details__tab__desc">
                      <h6>Products Infomation</h6>
                      <p>{filtered.information}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <FeaturedProducts featuredProductsInfo={fearutedFour} />
    </div>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  const res = await fetch("http://localhost:5001/products");
  const productData: ProductCard[] = await res.json();
  
  const paths = productData.map((c) => ({
    

    params: {
      id: c.id.toString(),
    },

  }));
  return {
    paths,
    fallback: false,

  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {


const myRes = await fetch("http://localhost:5001/products?_start=1&_limit=4")
const fearutedFour = await myRes.json()

  if (params && params.id) {
    const res = await fetch(`http://localhost:5001/products`);
    const product: ProductCard[] = await res.json();
    
    // console.log(`has items` , Object.keys(product))
    /// kako se proveruva dali e eden objekt prazen
    console.log(`has items` , Object.values(product))

    const filtered = product.find((c) => c.id === params.id);

    if (!filtered) {
      return { notFound: true };
    }

    return {
      props: {
        filtered,
        fearutedFour,
      },
    };
  }

  return { 
    props : {
      fearutedFour
    },
    notFound: true };
};

export default ShopDetail;
