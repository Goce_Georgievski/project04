import type { GetServerSideProps, GetStaticProps, NextPage } from "next";
import Head from "next/head";
import Breadcrumbs from "../../components/Breadcrumbs";
import { ProductCard } from "../../components/FeaturedProducts";
import Filters from "../../components/Filters";
import Product from "../../components/Product";

interface Props {
  product:ProductCard[]
  productCategoriesInfo:{
    name:string;
    slug:string;
  }[],
  productSizesInfo:{
    name:string;
    slug:string;
  }[],
}



const Shop: NextPage<Props> = ({product,productCategoriesInfo,productSizesInfo}) => {
  return (
    <div>
      <Head>
        <title>Shop</title>
      </Head>

      <Breadcrumbs />

      <section className="product spad">
        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-md-5">
              <Filters productCategoriesInfo={productCategoriesInfo} productSizesInfo={productSizesInfo}/>
            </div>
            <div className="col-lg-9 col-md-7">
              <div className="row">
               {product.length > 0 ? product.map(c=>(
                 <Product key={`productInfo-${c.id}`} id={c.id} description={c.description} filename={c.filename} information={c.information} price={c.price} size={c.size} title={c.title} type={c.type} />
               )): <p>There are no products based on your filter.</p>}

                {/* show this message if there are no results */}
                {/* <p>There are no products based on your filter.</p> */}
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};



export const getServerSideProps:GetServerSideProps = async({query})=>{

      let res
      let product:ProductCard[]

      const type = query.type
      const size = query.size


      if (type && size ) {

        res = await fetch(`http://localhost:5001/products?size_like=${size}&type_like=${type}`)
      } else if (type) {
        res = await fetch(`http://localhost:5001/products?type_like=${type}`)
      } else if (size ) {
        res = await fetch(`http://localhost:5001/products?size_like=${size}`)
      } else {
        res  = await fetch("http://localhost:5001/products") 
      }

    product = await res.json()


  // const resOne = await fetch("http://localhost:5001/products")
  // const productsCardInfo = await resOne.json()

  const resTwo = await fetch("http://localhost:5001/productCategories")
  const productCategoriesInfo = await resTwo.json()
  
  const resThree = await fetch("http://localhost:5001/productSizes")
  const productSizesInfo = await resThree.json()



  return{
    props:{
      // productsCardInfo,
      productCategoriesInfo,
      productSizesInfo,
      product,
    }
  }
}
export default Shop;
